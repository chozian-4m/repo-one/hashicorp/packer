# Packer

Packer is a tool for building identical machine images for multiple platforms
from a single source configuration. It is lightweight, runs on every major operating system, and is highly
performant, creating machine images for multiple platforms in parallel. Packer
comes out of the box with support for many platforms, the full list of which can
be found at https://www.packer.io/docs/builders/index.html. Support for other platforms can be added via plugins.

Usage
The repository automatically builds containers for using the packer command line program. It contains the full version of the build that compiles the binary from source inside the container before exposing it for use.

The full version of this container contains all of the source code found in the parent repository. Using Google's official golang image as a base, this container will copy the source from the master branch, build the binary, and expose it for running. Since the build is done on Docker Hub, the container is ready for use. Because all build artifacts are included, it should be quite a bit larger than the light image. This version of the container is useful for development or debugging.


## Documentation URLs

* [Website](https://www.packer.io)
* [Packer docs](https://www.packer.io/docs).
* [introduction and getting started guide](https://www.packer.io/intro)
* [download a pre-built Packerbinary](https://www.packer.io/downloads.html)
* [compile Packer yourself](https://github.com/hashicorp/packer/blob/master/.github/CONTRIBUTING.md#setting-up-go-to-work-on-packer).
