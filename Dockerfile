ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM hashicorp/packer:1.8.0 as base

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}


COPY --from=base /bin/packer /bin/packer

RUN dnf upgrade -y && \
    dnf clean all && \
    rm -rf /var/cache/dnf

RUN groupadd -g 1001 packer && \
    useradd -r -u 1001 -m -s /sbin/nologin -g packer packer && \
    ln -s /bin/packer /usr/local/bin/packer

USER packer

HEALTHCHECK --start-period=5s --timeout=5s \
  CMD curl -f http://localhost:8080 || exit 1

ENTRYPOINT ["/bin/packer"]
